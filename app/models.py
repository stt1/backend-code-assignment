"""
Serializable data model classes
"""
from abc import ABC
from flask.json import JSONEncoder


class MetaJsonEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Meta):
            return obj.json()

        return JSONEncoder.default(self, obj)


class Meta(ABC):
    def __init__(self, **kwargs):
        for key, val in kwargs.items():
            setattr(self, key, val)

    def json(self):
        return {key: getattr(self, key, None) for key in self.__slots__}


class Person(Meta):
    __slots__ = "id", "name", "birthdate", "city", "country"
