"""
Flask app_context services
"""
from datetime import datetime
from typing import List
import csv

from models import Person


class DataService:
    def __init__(self):
        self.persons = {}
        self.persons_by_country = {}

    def init(self, path: str):
        countries = {}
        with open(path + '/countries.csv') as fh:
            for row in csv.DictReader(fh):
                countries[row['city']] = row['country']

        with open(path + '/persons.csv') as fh:
            for row in csv.DictReader(fh):
                _id = int(row['id'])
                self.persons[_id] = Person(id=_id, name=row['name'])

        with open(path + '/birthdates.csv') as fh:
            for row in csv.DictReader(fh):
                _id = int(row['id'])
                self.persons[_id].birthdate = datetime.strptime(row['birthdate'], '%Y%m%d').date()

        with open(path + '/cities.csv') as fh:
            for row in csv.DictReader(fh):
                _id = int(row['id'])
                self.persons[_id].city = row['city']
                self.persons[_id].country = countries[row['city']]

        for person in self.persons.values():
            if person.country not in self.persons_by_country:
                self.persons_by_country[person.country] = []
            self.persons_by_country[person.country].append(person.id)

    def get_person_ids_by_country(self, country: str) -> List[int]:
        return self.persons_by_country[country]

    def get_person(self, person_id: int) -> Person:
        return self.persons[person_id]

    def get_persons(self) -> List[Person]:
        return self.persons.values()
