"""
Flask routes
"""
from datetime import date
from operator import attrgetter
from flask import Blueprint, current_app, request, jsonify

from utils import next_birthday


bp = Blueprint('person', __name__)


@bp.route('/<int:person_id>', methods=['GET'])
def get_person(person_id):
    try:
        result = current_app.data.persons[person_id]
        return jsonify(result)
    except KeyError:
        return 'Not found', 404


@bp.route('/', methods=['GET'])
def get_persons():
    page_size = 25
    country = request.args.get('country', type = str)
    page = request.args.get('page', default = 0, type = int)
    sort = request.args.get('sort', type = str)

    result = []
    if country:
        if country not in current_app.data.persons_by_country:
            return 'No such country', 404

        result = [current_app.data.get_person(i)
                  for i in current_app.data.get_person_ids_by_country(country)]
    else:
        result = list(current_app.data.get_persons())

    if sort == 'oldest':
        result = sorted(result, key=attrgetter('birthdate'))
    elif sort == 'youngest':
        result = sorted(result, key=attrgetter('birthdate'), reverse=True)

    offset = page_size * page
    max_page = int((len(result)-1) / page_size)
    result = result[offset:offset+page_size]

    return jsonify(page=page, max_page=max_page, result=result)


@bp.route('/nextbirthdate', methods=['GET'])
def get_person_with_next_bday():
    today = date.today()
    result = min(current_app.data.persons.values(),
                 key=lambda p: abs(today - next_birthday(p.birthdate)))
    return jsonify(result)
