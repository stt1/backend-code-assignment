"""
backend-code-assignment
"""
from flask import Flask
from services import DataService
from routes.person import bp as person_bp
import models


app = Flask(__name__)
app.register_blueprint(person_bp, url_prefix='/person')
app.json_encoder = models.MetaJsonEncoder


def run():
    with app.app_context():
        app.data = DataService()
        app.data.init('./datafiles')
        app.run(host='0.0.0.0')


if __name__ == '__main__':
    run()
