"""
Utility functions
"""
from datetime import date


def next_birthday(dob):
    today = date.today()
    try:
        birthday = dob.replace(year = today.year)
    except ValueError:
        # leap year
        birthday = dob.replace(year = today.year,
                               month = dob.month + 1, day = 1)

    if birthday < today:
        birthday.replace(year = today.year + 1)
    return birthday
