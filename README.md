# Code assignment: backend developer

In the [datafiles](/datafiles) directory there are some files containing persons, with data like birth date and which city they live in.

Your task is to take the data and make it accessible through a web API as JSON response.

 - Create a `Person` class containing all the data for a person, with properties like `age`, `first_name`, and `country`
 - Create an endpoint that returns one (1) Person based on their ID number
 - Create endpoint(s) through which the following can be fetched. Return at most 25 persons. It should also be possible to get the next/previous 25 persons (ie. pagination) of the whole result:
   - All Persons for a specific country 
   - All Persons, sorted from oldest -> youngest
   - All Persons, sorted from youngest -> oldest
 - Create an endpoint from which the Person(s) with the next birthday can be retrieved

## Technologies

 - Python 3
 - [Flask](https://flask.palletsprojects.com/)
 - Do not use a real database

## Completion

Push the completed task to a private Git repository — on Github, Bitbucket, Gitlab, or whichever service you prefer. We like [gitlab](https://gitlab.com).

## Docker

    docker build -t be-app .
    docker run -p 5000:5000 --rm be-app
